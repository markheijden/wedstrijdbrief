<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wedstrijdbriefjes Beach</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            background-color: grey;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .upload-container {
            background-color: white; /* Set the container background to white */
            padding: 40px;
            width: 35vw;
            min-width: 300px !important;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            border-radius: 8px;
            text-align: center;
        }

        .upload-container input[type="file"] {
            margin-top: 20px;
        }
    </style>
</head>
<body>

    <div class="upload-container">
        <h2>Wedstrijdbriefjes generator</h2>
        <form action="genereer.php" method="POST" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="fileUpload" class="form-label">Kies een Excel bestand (.xlsx)</label>
                <input type="file" class="form-control" id="fileUpload" name="schema">
            </div>
            <button type="submit" class="btn btn-primary">Maak wedstrijdbriefjes</button>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>