<?php
error_reporting(E_ERROR | E_PARSE);

require __DIR__ . '/vendor/autoload.php';

$bestand = $_FILES['schema']['tmp_name'];

if (!empty($bestand)) {
    $wedstrijden = haalDataUitExcelBestand($bestand);
    genereerPdf($wedstrijden);
}

function haalDataUitExcelBestand(string $bestand): array
{
    $wedstrijden = [];
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($bestand);
    $worksheet = $spreadsheet->getActiveSheet();

    $data = $worksheet->toArray();
    $wedstrijden = [];

    foreach ($data as $wedstrijd) {

        if (empty($wedstrijd[0]) && empty($wedstrijd[1]) && empty($wedstrijd[2]) && empty($wedstrijd[3])) {
            continue;
        }

        $wedstrijd = [
            'dag' => $wedstrijd[0],
            'tijd' => $wedstrijd[1],
            'team1' => $wedstrijd[2],
            'team2' => $wedstrijd[3],
            'veld' => $wedstrijd[4],
            'klasse' => $wedstrijd[5],
            'scheidsrechter' => $wedstrijd[6],
        ];

        $wedstrijden[] = $wedstrijd;
    }

    return $wedstrijden;
}

function genereerPdf(array $wedstrijden)
{
    $html2pdf = new Spipu\Html2Pdf\Html2Pdf('L');

    $html = <<<EOD
    <style type="text/Css">
    * {
        font-family: robotomono;
        box-sizing: border-box;
        font-size: 16px;
    }

    h2 {
        margin-top: 0;
        margin-bottom: 30px;
    }

    .infoBlok {
        position: relative;
        width: 50%;
        text-align: center;
        padding: 20px 5px;
        background-color: #eee;
        vertical-align: top;
    }

    .alignCenter {
        text-align: center;
    }

    strong {
        font-family: robotomonob;
        font-size: 19px;
    }

    table {
        border-collapse: collapse;
    }
    </style>
    EOD;

    foreach (array_chunk($wedstrijden, 2) as $wedstrijd) {
        if (empty($wedstrijd[1]['dag'])) {
            $wedstrijd[1] = [
                'dag' => '&nbsp;',
                'tijd' => '&nbsp;',
                'klasse' => '&nbsp;',
                'veld' => '&nbsp;',
                'scheidsrechter' => '&nbsp;',
            ];
        }
        

        $html .= <<<EOD
        
        <page style="font-size: 18px">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 50%; padding: 20px; vertical-align: top;">
                        <table style="width: 100%;">
                            <tr>
                                <td colspan="2" class="alignCenter">
                                    <img src="./images/logo.png" style="width:40%;" />
                                    <h2>Beachvolleybal toernooi</h2>
                                </td>
                            </tr>
                            <tr>
                                <td class="infoBlok" style="border-right:7px solid white;">
                                    <strong>Dag:</strong><br>
                                    {$wedstrijd[0]["dag"]}
                                </td>
                                <td class="infoBlok" style="border-left:7px solid white;">
                                    <strong>Tijd:</strong><br>
                                    {$wedstrijd[0]["tijd"]}
                                </td>
                            </tr>
                            <tr><td colspan="2" style="height:5px;"></td></tr>
                            <tr>
                                <td class="infoBlok" style="border-right:7px solid white;">
                                    <strong>Klasse:</strong><br>
                                    {$wedstrijd[0]["klasse"]}
                                </td>
                                <td class="infoBlok" style="border-left:7px solid white;">
                                    <strong>Veld:</strong><br>
                                    {$wedstrijd[0]["veld"]}
                                </td>
                            </tr>
                            <tr><td colspan="2" style="height:5px;"></td></tr>
                            
                            <tr>
                                <td class="infoBlok" style="border-right:7px solid white;">
                                    <strong>Scheidsrechter:</strong><br>
                                    {$wedstrijd[0]["scheidsrechter"]}
                                </td>
                                <td class="infoBlok" style="border-left:7px solid white;">
                                    <strong>Uitslag:</strong><br>
                                    _
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <table style="width: 100%; text-align:center; padding-top: 40px;">
                                        <tr>
                                            <td style="width: 50%;">
                                                <strong>Team 1</strong>
                                            </td>
                                            <td style="width: 50%;">
                                                <strong>Team 2</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50%; height: 70px; vertical-align: top;">
                                                {$wedstrijd[0]["team1"]}
                                            </td>
                                            <td style="width: 50%; height: 70px; vertical-align: top;">
                                                {$wedstrijd[0]["team2"]}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50%; height: 80px; vertical-align: top; padding-bottom: 20px;">
                                                ..............<br><br><br><br>
                                                ..............
                                            </td>
                                            <td style="width: 50%; height: 80px; vertical-align: top; padding-bottom: 20px;">
                                                ..............<br><br><br><br>
                                                ..............
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 50%; padding: 20px; vertical-align: top;">
                        <table style="width: 100%;">
                            <tr>
                                <td colspan="2" class="alignCenter">
                                    <img src="./images/logo.png" style="width:40%;" />
                                    <h2>Beachvolleybal toernooi</h2>
                                </td>
                            </tr>
                            <tr>
                                <td class="infoBlok" style="border-right:7px solid white;">
                                    <strong>Dag:</strong><br>
                                    {$wedstrijd[1]["dag"]}
                                </td>
                                <td class="infoBlok" style="border-left:7px solid white;">
                                    <strong>Tijd:</strong><br>
                                    {$wedstrijd[1]["tijd"]}
                                </td>
                            </tr>
                            <tr><td colspan="2" style="height:5px;"></td></tr>
                            <tr>
                                <td class="infoBlok" style="border-right:7px solid white;">
                                    <strong>Klasse:</strong><br>
                                    {$wedstrijd[1]["klasse"]}
                                </td>
                                <td class="infoBlok" style="border-left:7px solid white;">
                                    <strong>Veld:</strong><br>
                                    {$wedstrijd[1]["veld"]}
                                </td>
                            </tr>
                            <tr><td colspan="2" style="height:5px;"></td></tr>
                            <tr>
                                <td class="infoBlok" style="border-right:7px solid white;">
                                    <strong>Scheidsrechter:</strong><br>
                                    {$wedstrijd[1]["scheidsrechter"]}
                                </td>
                                <td class="infoBlok" style="border-left:7px solid white;">
                                    <strong>Uitslag:</strong><br>
                                    _
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="2">
                                    <table style="width: 100%; text-align:center; padding-top: 40px;">
                                        <tr>
                                            <td style="width: 50%;">
                                                <strong>Team 1</strong>
                                            </td>
                                            <td style="width: 50%;">
                                                <strong>Team 2</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50%; height: 70px; vertical-align: top;">
                                                {$wedstrijd[1]["team1"]}
                                            </td>
                                            <td style="width: 50%; height: 70px; vertical-align: top;">
                                                {$wedstrijd[1]["team2"]}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50%; height: 80px; vertical-align: top; padding-bottom: 20px;">
                                                ..............<br><br><br><br>
                                                ..............
                                            </td>
                                            <td style="width: 50%; height: 80px; vertical-align: top; padding-bottom: 20px;">
                                                ..............<br><br><br><br>
                                                ..............
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </page>
        EOD;
    }


    $html2pdf->writeHTML($html);
    $html2pdf->output();
}
