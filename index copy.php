<?php

require __DIR__ . '/vendor/autoload.php';

$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Mark van der Heijden');
$pdf->SetTitle('Wedstrijdbrief');
$pdf->SetSubject('SicPugno beachvolleybal toernooi');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(0, 0, 0);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, 0);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('robotomono', '', 14);

// add a page
$pdf->AddPage();

// set some text to print
$html = <<<EOD
<style>
* {
    box-sizing: border-box;
    border: 1px solid red;
}

.alignCenter{
    text-align: center;
}

table,
table tr,
tr td,
table tbody,
table thead,
table tfoot,
table tr th,
table tfoot tr tf
{
    margin:0;
    padding:0;
    background:none;
    border-collapse:collapse;
    border-spacing:0;
    background-image:none;
}
</style>
<table height="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td width="50%">
            <table>
                <tr>
                    <td colspan="2" class="alignCenter">Sic Pugno beachvolleybal toernooi</td>
                </tr>
                <tr>
                    <td colspan="2" class="alignCenter" height="50" valign="middle"><strong>Veld:</strong> Goossens</td>
                </tr>
                <tr>
                    <td>
                        <span>Klasse:</strong><br>
                        Heren 3 x 3 Poule A
                    </td>
                    <td height="100">
                        <table>
                            <tr>
                                <td>
                                    <strong>Tijd:</strong>
                                </td>
                                <td colspan="2">
                                    <strong>Zaterdag</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>14:00</td>
                                <td>-</td>
                                <td>14:20</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <strong>Team 1</strong><br>
                                    Berrege met stadsie <br><br>
                                    ..........................<br><br>
                                    ..........................
                                </td>
                                <td>
                                    <strong>Team 2</strong><br>
                                    Lifeplus <br><br>
                                    ..........................<br><br>
                                    ..........................
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%">

        </td>
    </tr>
</table>
EOD;

// print a block of text using Write()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_002.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+